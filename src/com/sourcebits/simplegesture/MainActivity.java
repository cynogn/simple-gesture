package com.sourcebits.simplegesture;

import java.util.ArrayList;
import java.util.Random;

import android.app.Activity;
import android.app.ProgressDialog;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements
		OnGesturePerformedListener {
	TextView text;
	TextView toWin;
	int MaxNumber = (new Random().nextInt(100) % 25);
	int number = 0;
	int index = 0;
	ArrayList<Integer> answers = new ArrayList<Integer>();
	private GestureLibrary mLibrary;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
		text = (TextView) findViewById(R.id.textView1);
		toWin = (TextView) findViewById(R.id.toWin);
		CpuInitialize(MaxNumber);
		toWin.setText(MaxNumber + "");
		text.setText(number + "");

		if (!mLibrary.load()) {
			finish();
		}
		GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.gestures);
		gestures.addOnGesturePerformedListener(this);

	}

	private void CpuInitialize(int value) {
		int firstNumber = value % 4;
		Log.v("VALUES", "" + (int) (firstNumber));
		while (firstNumber + 4 <= MaxNumber) {
			firstNumber += 4;
			answers.add(firstNumber);
			Log.v("VALUES", "" + (int) firstNumber);
		}
	}

	public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
		ArrayList<Prediction> predictions = mLibrary.recognize(gesture);
		if (number <= MaxNumber) {
			if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
				String result = predictions.get(0).name;
				if ("ONE".equalsIgnoreCase(result)) {
					number += 1;
				} else if ("TWO".equalsIgnoreCase(result)) {
					number += 2;
				} else if ("THREE".equalsIgnoreCase(result)) {
					number += 3;
				}
				text.setText(number + "");
				GameOver();
				new asyncTask().execute();
			}
		}
	}

	private void GameOver() {
		if (number >= MaxNumber) {
			Toast.makeText(MainActivity.this, "HE..HE..YOU Loser!! ",
					Toast.LENGTH_SHORT).show();
		}
	}

	class asyncTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog progress;

		@Override
		protected void onPreExecute() {
			progress = new ProgressDialog(MainActivity.this);
			progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progress.setCancelable(true);
			progress.setTitle("CPU");
			progress.setMessage("I AM THINKING..");
			progress.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				Thread.sleep(1000);
				if (answers.size() > index) {
					number = answers.get(index);
					index++;
					Log.v("CPU", "" + number);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			progress.dismiss();
			text.setText(number + "");
			GameOver();
			super.onPostExecute(result);
		}
	}

}
